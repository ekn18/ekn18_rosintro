'''
Emma Nisbet 

Writing capital E with the End Effector in Cartesian and Move to Path

The imports and try/except block are from: https://ros-planning.github.io/moveit_tutorials/doc/move_group_python_interface/move_group_python_interface_tutorial.html#

That link was also referenced by me throughout. 
'''

from __future__ import print_function
from six.moves import input
import time

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list



def initialize():
    '''
    This function initializes all necessary objects for the robot to move.

    It returns move_group, a MoveGroupCommander object,
    used in later functions to plan and execute motions.
    '''

    #Initialize moveit_commander and rospy node
    moveit_commander.roscpp_initialize(sys.argv)
    rospy.init_node("HW6_Q4_motion", anonymous=True)

    ## Instantiate a `MoveGroupCommander`_ object. 
    group_name = "manipulator"
    move_group = moveit_commander.MoveGroupCommander(group_name)

    return move_group


def starting_position(move_group):
    '''
    Input: move_group, a MoveGroupCommander Object

    This function moves the robot out of its zero configuration and
    to its initial position, allowing it to draw a capital E without getting stuck. 

    Note: tau = 2pi
    
    '''
    print("moving to initial position...")

    joint_goal = move_group.get_current_joint_values()

    #move out of zero configuration 
    joint_goal[0] = 0
    joint_goal[1] = -tau / 8
    joint_goal[2] = 0
    joint_goal[3] = -tau / 4
    joint_goal[4] = 0
    joint_goal[5] = tau / 6  
    move_group.go(joint_goal, wait=True)

    #move to starting position to begin drawing
    joint_goal[0] = -tau/4
    joint_goal[1] = -tau/8
    joint_goal[2] = tau/4
    joint_goal[3] = 0
    joint_goal[4] = -tau/32

    move_group.go(joint_goal, wait=True)
    move_group.stop()

def E_movement_cartesian(move_group):
    '''
    input: move_group, a MoveGroupCommander Object

    This function plans and executes a Cartesian path (specifically drawing an E) 
    directly by specifying a list of waypoints for the end-effector to go through.

    (uses code from plan_cartesian_path)
    '''

    print("Cartesian path: starting to draw E in 3 seconds...")
    time.sleep(3) #pause for 3 seconds

    waypoints = []

    #steps to move current pose
    wpose = move_group.get_current_pose().pose
    
    wpose.position.y += -0.15  #move sideways (y) - bottom line of E
    waypoints.append(copy.deepcopy(wpose))

    wpose.position.z +=  0.1  # move up (z) - move up to the second line of E
    waypoints.append(copy.deepcopy(wpose))

    wpose.position.y +=  0.15  # move sideways(y) - middle line of E
    waypoints.append(copy.deepcopy(wpose))

    wpose.position.y += -.15  # move back - trace back over the middle line
    waypoints.append(copy.deepcopy(wpose))

    wpose.position.z +=  0.1  # move up (z) - move towards top line
    waypoints.append(copy.deepcopy(wpose))

    wpose.position.y += 0.15  # move sideways(y) - draw the top line of E
    waypoints.append(copy.deepcopy(wpose))


    #Cartesian path interpolated at a resolution of .1 cm so eef_step = .01 in Cartesian translation. 
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0) #waypoints to follow, eef_step, #jump threshold


    move_group.execute(plan, wait=True)

    move_group.stop()


def E_move_to_pose(move_group):
    '''
    input: move_group, a MoveGroupCommander Object

    This function draws an E with the end-effector using code from the go_to_pose function
    to move the EE to the desired pose. 
    '''
    
    print("Go_to_pose: Move to initial position...")
    #starting x,y,z positions 
    startx = 0.19162251825382676 - .1
    starty = -0.5093165777944151 +.15 
    startz = 0.042693282526320375 +.1
    
    pose_goal = geometry_msgs.msg.Pose()

    #move to starting position
    pose_goal.orientation.w = 1.0
    pose_goal.position.x =startx#red
    pose_goal.position.y = starty #green
    pose_goal.position.z = startz#.blue    
    move_group.set_pose_target(pose_goal)
    success = move_group.go(wait=True)

    print("Go_to_pose: start drawing...")

    #draw bottom line of E
    pose_goal.position.y = starty - .15 
    move_group.set_pose_target(pose_goal)
    success = move_group.go(wait=True)

    #move up to second line in E

    pose_goal.position.z = startz + .1
    move_group.set_pose_target(pose_goal)
    success = move_group.go(wait=True)

    #draw second line in E
    pose_goal.position.y = starty  

    move_group.set_pose_target(pose_goal)
    success = move_group.go(wait=True)

    #traceback second line of E
    pose_goal.position.y = starty -.15 

    move_group.set_pose_target(pose_goal)
    success = move_group.go(wait=True)

    #move up to top line of E
  
    pose_goal.position.z = startz + .2
    move_group.set_pose_target(pose_goal)
    success = move_group.go(wait=True)

    #draw top line of E
    pose_goal.position.y = starty  

    move_group.set_pose_target(pose_goal)
    success = move_group.go(wait=True)


def main():
    move_group = initialize() 
    starting_position(move_group)
    E_movement_cartesian(move_group)
    E_move_to_pose(move_group)
    

main()
  

    

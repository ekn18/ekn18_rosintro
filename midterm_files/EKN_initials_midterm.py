'''
Emma Kate Nisbet 

Writing my initials, E K N, with the End Effector 

The imports and try/except block are from: https://ros-planning.github.io/moveit_tutorials/doc/move_group_python_interface/move_group_python_interface_tutorial.html#

The link was also referenced by me throughout. 
'''

from __future__ import print_function
import time

import sys
import copy
import rospy
import moveit_commander


#initialize tau
try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi




def initialize():
    '''
    This function initializes all necessary objects for the robot to move.

    It returns move_group, a MoveGroupCommander object,
    used in later functions to plan and execute motions.
    '''

    #Initialize moveit_commander and rospy node
    moveit_commander.roscpp_initialize(sys.argv)
    rospy.init_node("HW6_E_motion", anonymous=True)

    ## Instantiate a `MoveGroupCommander`_ object. 
    group_name = "manipulator"
    move_group = moveit_commander.MoveGroupCommander(group_name)

    return move_group


def starting_position(move_group):
    '''
    Input: move_group, a MoveGroupCommander Object

    This function first moves the robot out of its zero configuration (a singularity)
    then it moves to its initial drawing position. This position allows the robot to draw 
    the initials without getting stuck. 

    Note: tau = 2pi
    
    '''
    print("moving out of singularity...")

    joint_goal = move_group.get_current_joint_values()

    #move out of zero configuration so I can move to next position 
    joint_goal[0] = 0
    joint_goal[1] = -tau / 8
    joint_goal[2] = 0
    joint_goal[3] = -tau/4 
    joint_goal[4] = 0
    joint_goal[5] = tau / 6 
    move_group.go(joint_goal, wait=True) #execute the joint values
    

    #setting the same joint values
    #sometimes the robot gets an error the first time they're set so this fixes it
    joint_goal[0] = 0
    joint_goal[1] = -tau / 8
    joint_goal[2] = 0
    joint_goal[3] = -tau/4 
    joint_goal[4] = 0
    joint_goal[5] = tau / 6 
    move_group.go(joint_goal, wait=True)
    move_group.stop()
    time.sleep(5) #pause for 5s to allow the robot to move to the position

    
    print("moving to starting E pose...")
    #move to drawing start position to begin drawing
    joint_goal[0] = -tau/4
    joint_goal[1] = -tau/8
    joint_goal[2] = tau/4
    joint_goal[3] = 0
    joint_goal[4] = -tau/32
    move_group.go(joint_goal, wait=True)
    move_group.stop()



def E_movement(move_group):
    '''
    Input: move_group, a MoveGroupCommander Object

    This function plans and executes a Cartesian path (specifically drawing an E) 
    directly by specifying a list of waypoints for the end-effector to go through.
    '''

    print("starting E movement in 2 seconds...")
    time.sleep(2) #pause for 2 seconds

    waypoints = []

    #steps to move current pose
    wpose = move_group.get_current_pose().pose
    
    #move sideways (y) - bottom line of E
    wpose.position.y += -0.15  
    waypoints.append(copy.deepcopy(wpose))

    # move up (z) - move up to the second line of E
    wpose.position.z +=  0.1  
    waypoints.append(copy.deepcopy(wpose))

    # move sideways(y) - middle line of E
    wpose.position.y +=  0.15  
    waypoints.append(copy.deepcopy(wpose))

    # move back - trace back over the middle line
    wpose.position.y += -.15  
    waypoints.append(copy.deepcopy(wpose))

    # move up (z) - move towards top line
    wpose.position.z +=  0.1 
    waypoints.append(copy.deepcopy(wpose))

    # move sideways(y) - draw the top line of E
    wpose.position.y += 0.15  
    waypoints.append(copy.deepcopy(wpose))


    #Cartesian path interpolated at a resolution of .1 cm so eef_step = .01 in Cartesian translation. 
    (plan, _) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0) #waypoints to follow, eef_step, #jump threshold


    move_group.execute(plan, wait=True)


def K_movement(move_group):
    '''
    Input: move_group, a MoveGroupCommander Object

    This function plans and executes a Cartesian path (specifically drawing an K) 
    directly by specifying a list of waypoints for the end-effector to go through.
    '''

    time.sleep(2)
    waypoints = []
    print("moving to initial K pose...")

    #steps to move current pose
    wpose = move_group.get_current_pose().pose

    #move back to original drawing position where E started
    wpose.position.y += -0.1 
    wpose.position.z +=  -0.2 
    waypoints.append(copy.deepcopy(wpose))
   
    print("starting K movement...")

    # draw line of K
    wpose.position.z +=  0.2 
    waypoints.append(copy.deepcopy(wpose))

    #go halfway down the line to prepare to draw the diagonals of K
    wpose.position.z +=  -0.1 
    waypoints.append(copy.deepcopy(wpose))

    #draw top diagonal of K 
    wpose.position.y += 0.1 
    wpose.position.z +=  0.1
    waypoints.append(copy.deepcopy(wpose))

    #draw back over top diagonal of K 
    wpose.position.y += -0.1 
    wpose.position.z +=  -0.1
    waypoints.append(copy.deepcopy(wpose))

    #draw bottom diagonal of K 
    wpose.position.y += 0.1 
    wpose.position.z +=  -0.1
    waypoints.append(copy.deepcopy(wpose))

    #draw back over bottom diagonal line of K
    wpose.position.y += -0.1 
    wpose.position.z +=  0.1
    waypoints.append(copy.deepcopy(wpose))

    #Cartesian path interpolated at a resolution of .1 cm so eef_step = .01 in Cartesian translation. 
    (plan, _) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0) #waypoints to follow, eef_step, #jump threshold

    move_group.execute(plan, wait=True)



def N_movement(move_group):
    '''
    input: move_group, a MoveGroupCommander Object

    This function plans and executes a Cartesian path (specifically drawing an N) 
    directly by specifying a list of waypoints for the end-effector to go through.
    '''


    time.sleep(2)
    waypoints = []

    print("moving to initial N pose...")
    wpose = move_group.get_current_pose().pose
    
    #move back to original position where K started
    wpose.position.z +=  -0.1 
    waypoints.append(copy.deepcopy(wpose))

    print("starting N movement...")
    #move up in z and draw the first horizontal line of capital N
    wpose.position.z +=  .2 
    waypoints.append(copy.deepcopy(wpose))

    #diagonal line going down 
    wpose.position.y+= .075 
    wpose.position.z +=  -.2
    waypoints.append(copy.deepcopy(wpose))

    #third line of N moving up in z 
    wpose.position.z +=  .2 
    waypoints.append(copy.deepcopy(wpose))


    #Cartesian path interpolated at a resolution of .1 cm so eef_step = .01 in Cartesian translation. 
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0) #waypoints to follow, eef_step, #jump threshold


    move_group.execute(plan, wait=True)



def main():
    move_group = initialize() 
    starting_position(move_group)
    E_movement(move_group)
    K_movement(move_group)
    N_movement(move_group)

main()
  

    

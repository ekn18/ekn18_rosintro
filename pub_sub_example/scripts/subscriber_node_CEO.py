#!/usr/bin/env python3
import rospy
from std_msgs.msg import String

def callback(data):
    rospy.loginfo("CEO: Received: %s", data.data)

def subscriber_node_CEO():
    rospy.init_node('subscriber_node_CEO')
    rospy.Subscriber('topic_c', String, callback)
    rospy.spin()

if __name__ == '__main__':
    subscriber_node_CEO()

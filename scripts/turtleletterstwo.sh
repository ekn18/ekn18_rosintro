#!/usr/bin/bash

rosservice call /clear

rosservice call /turtle1/teleport_absolute 1.5 5.5 0

rosservice call /turtle1/set_pen 70 50 50 5 0

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[4, 0.0,0.0]' '[0.0,0.0,0.0]'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[0.1,0.0,0.0]' '[0.0,0.0,2.0]'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[9.0,0.0,0.0]' '[0.0,0.0,5.0]'

rosservice call /spawn 6 3 1.2 'turtle2'

rosservice call /turtle2/set_pen 255 0 0 5 0 

rosservice call /turtle2/teleport_absolute 7 6 0

rosservice call /turtle2/teleport_absolute 8 5 0

rosservice call /turtle2/teleport_absolute 9 6 0

rosservice call /turtle2/teleport_absolute 10 3 0 
